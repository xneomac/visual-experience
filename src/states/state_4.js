import anime from 'animejs';
import { state } from "./state";

const duration = 200;

export class state_4  {


    constructor () {
        this.prec_value = 0.1;

        this.animation_bg = anime({
            targets: '.bg_state_4',
            opacity: 1,
            scale: 1.3,
            duration: 200,
            autoplay: false
        })
    }

    run (analysers) {
        return new Promise((resolve, reject) => {

            const analyser = analysers.left;

            let buffer_length = analyser.frequencyBinCount;
            let data_array = new Float32Array(buffer_length);
            analyser.getFloatFrequencyData(data_array);

            let max = Math.max(...data_array) + 60;
            this.seek_index = (Math.max(max, 0.1) / 315) * this.duration;
            this.animation_bg.seek(this.prec_value);

            this.prec_value = max;
            resolve();
        });

    }

    enter () {
        console.log('Enter State 5');
    }

    exit () {
        anime({
            targets: '.bg_state_4',
            opacity: 0,
            scale: 1
        });
    }
}
