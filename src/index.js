import record_config from './recorder';
import { state_1 } from './states/state_1.js';
import { state_2 } from './states/state_2';
import { state_3 } from './states/state_3';
import { state_4 } from './states/state_4';
import { state_5 } from './states/state_5';

const tick_time = 40; // delta Time in ms

let animations = [];

const all_states = new Array(
  state_1,
  state_2,
  state_3,
  state_4,
  state_5
);

console.log(all_states);
console.log(all_states[0]);

let current_state = new all_states[0]();

/*
for(let i = 1; i < 5; ++i) {
  let anim = anime({
    targets: '#tile_' + i,
    translateX: function(el) {
      return el.getAttribute('data-x');
    },
    translateY: function(el) {
      return el.getAttribute('data-y');
    },
    scale: 0.5,
    elasticity: 200,
    autoplay: false
  });

  animations.push(anim);
}


function animation_1 () {
  console.log("Animation 1");
  anime({
      targets: '.movable',
      translateX: [
          { value: 100, duration: 1200 },
          { value: 0, duration: 1200 }
        ]
  });
}*/


document.addEventListener('keypress', (event)=> {
  console.log('Key ',event.key );

  let index = parseInt(event.key);

  if (index != NaN && index < all_states.length) {
    current_state.exit();
    console.log('Index', index);
    current_state = new all_states[index]();
    current_state.enter();
  }

  
});


let prec_tick = Date.now();
let delta = 1000 / 60;

function fps_manager() {
  return new Promise((resolve, reject) => {
    let current_tick = Date.now();
    let expected_next_tick = prec_tick + delta;
    if (current_tick > (expected_next_tick)) {
      prec_tick = current_tick;
      resolve();
    } else {
      window.setTimeout(()=> {
        prec_tick = current_tick;
        resolve();

      }, expected_next_tick - current_tick);
    }

  });
}

function update (analyzers) {
  fps_manager()
    .then(() =>   current_state.run(analyzers))
    .then(() => update(analyzers));
}

record_config()
  .then((analyzers)=> {
    update (analyzers);
});

