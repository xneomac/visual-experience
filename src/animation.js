import anime from 'animejs'

export function animation_1 () {
    console.log("Animation 1");
    anime({
        targets: '#tile_1',
        translateX: [
            { value: 100, duration: 1200 },
            { value: 0, duration: 800 }
          ],
          rotate: '1turn',
          backgroundColor: '#FFF',
          duration: 2000,
          loop: true
    })
}